package cruzeirao;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Gol {
	
	@Id
	private int id;
	private int tempo;
	private boolean penalty;
	
	public int getTempo() {
		return tempo;
	}
	public void setTempo(int tempo) {
		this.tempo = tempo;
	}
	public boolean isPenalty() {
		return penalty;
	}
	public void setPenalty(boolean penalty) {
		this.penalty = penalty;
	}

}
