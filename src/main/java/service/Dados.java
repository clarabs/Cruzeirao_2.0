package service;

import java.util.ArrayList;

import cruzeirao.Campeonato;
import cruzeirao.Categoria;
import cruzeirao.Equipe;
import cruzeirao.Gol;
import cruzeirao.Partida;
import cruzeirao.Usuario;


public class Dados {
	public static final ArrayList <Usuario> USUARIOS = new ArrayList <Usuario>();
	public static final ArrayList <Campeonato> CAMPEONATOS = new ArrayList <Campeonato>();
	public static final ArrayList <Categoria> CATEGORIAS= new ArrayList <Categoria>();
	public static final ArrayList <Equipe> EQUIPES = new ArrayList <Equipe>();
	public static final ArrayList <Gol> GOLS = new ArrayList <Gol>();
	public static final ArrayList <Partida> PARTIDAS = new ArrayList <Partida>();
	
	
}
