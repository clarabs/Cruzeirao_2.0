package service;

import java.util.ArrayList;
import java.util.List;


import cruzeirao.Gol;

public class GolService {
	private ArrayList<Gol> gols = Dados.GOLS;
	
	public List<Gol> getGols(){
		return gols;
	}
	
	public void salvar(Gol gol) {
		gols.add(gol);
	}
	
	public Gol getGolByTempo(Integer tempoGol) {
		
		for(int i=0; i< gols.size(); i++)
			if(tempoGol == gols.get(i).getTempo())
				return gols.get(i);
		
		return null;
	}

}
