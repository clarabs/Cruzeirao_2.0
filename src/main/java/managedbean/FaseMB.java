package managedbean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import cruzeirao.Equipe;
import cruzeirao.Fase;
import service.FaseService;

@ManagedBean
@SessionScoped

public class FaseMB {
	
	/*private ArrayList <Fase> lista = 
			new ArrayList<Fase>();*/
	
	private Fase novaFase = new Fase();
	private FaseService faseService = new FaseService();
	
	public String salvar() {
		faseService.salvar(novaFase);
		novaFase = new Fase();
		return "fasetabela";
	}
	 
	public List<Fase> getFases() {
		return faseService.getFases();
	}

	/*public ArrayList<Fase> getLista() {
		return lista;
	}

	public void setLista(ArrayList<Fase> lista) {
		this.lista = lista;
	}*/


	public Fase getNovaFase() {
		return novaFase;
	}

	public void setFase(Fase novaFase) {
		this.novaFase = novaFase;
	}

	

}
