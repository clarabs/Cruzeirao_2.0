package managedbean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import cruzeirao.Grupo;
import service.GrupoService;

@ManagedBean
@SessionScoped
public class GrupoMB {
	private Grupo novoGrupo = new Grupo();
	private GrupoService grupoService = new GrupoService();
	
	public String salvar() {
		grupoService.salvar(novoGrupo);
		novoGrupo = new Grupo();
		return "grupotabela";
	}
	 
	public List<Grupo> getGrupos() {
		return grupoService.getGrupos();
	}

	public Grupo getNovoGrupo() {
		return novoGrupo;
	}

	public void setGrupo(Grupo novoGrupo) {
		this.novoGrupo = novoGrupo;
	}
}
