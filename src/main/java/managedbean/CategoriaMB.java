package managedbean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import cruzeirao.Categoria;
import cruzeirao.Usuario;
import service.CategoriaService;


@ManagedBean
@SessionScoped

public class CategoriaMB {
	
	private Categoria categoria = new Categoria();
	private Categoria categoriaAtual;
	private CategoriaService categoriaService = new CategoriaService();
	
	public String salvar() {
		categoriaService.salvar(categoria);
		categoria = new Categoria();
		return "categoriatabela";
	}
	 

	public List<Categoria> getCategorias() {
		return categoriaService.getCategorias();
	}

	public Categoria getcategoria() {
		return categoria;
	}

	public void setcategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	public String gerenciar(Categoria categoria) {
		categoriaAtual = (Categoria) categoriaService.getCategorias();
		return "gerenciador";
	}

}