package managedbean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import cruzeirao.Partida;
import service.PartidaService;

@ManagedBean
@SessionScoped
public class PartidaMB {
	private Partida novaPartida = new Partida();
	private PartidaService partidaService = new PartidaService();
	
	public String salvar() {
		partidaService.salvar(novaPartida);
		novaPartida = new Partida();
		return "partidatabela";
	}
	 
	public List<Partida> getPartidas() {
		return partidaService.getPartidas();
	}

	public Partida getNovaPartida() {
		return novaPartida;
	}

	public void setPartida(Partida novaPartida) {
		this.novaPartida = novaPartida;
	}
}
