package managedbean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.RowEditEvent;

import cruzeirao.Usuario;
import service.UsuarioService;

@ManagedBean
@SessionScoped

public class UsuarioMB {
	
	private Usuario novoUsuario = new Usuario();
	public Usuario usuarioAtual;
	private UsuarioService usuarioService = new UsuarioService();
	private List<Usuario> usuarios;
	
	public String salvar() {
		//usuarioService.save(novoUsuario);
		//novoUsuario = new Usuario();
		//usuarioService.closeEntityManager();
		novoUsuario = usuarioService.save(novoUsuario);
		
		if(usuarios != null) {
			usuarios.add(novoUsuario);
		}
		
		novoUsuario = new Usuario();
		return "usuariotabela";
	}
	
	public void update(RowEditEvent event) {
		
		Usuario u = ((Usuario) event.getObject());
		novoUsuario = usuarioService.save(u);
		//novoUsuario = new Usuario();
	}
	 

	public List<Usuario> getUsuarios() {
		//List<Usuario> list = usuarioService.getAll(Usuario.class);
		//usuarioService.closeEntityManager();
		return usuarioService.getAll(Usuario.class);
	}
	
	
	public void remover(Usuario usuario) {
		//novoUsuario = usuarioService.getById(Usuario.class, usuario.getEmail());
		usuarioService.remove(usuario);
		//usuarios.remove(novoUsuario);
		//novoUsuario = new Usuario();
	}

	public Usuario getNovoUsuario() {
		return novoUsuario;
	}

	public void setNovoUsuario(Usuario novoUsuario) {
		this.novoUsuario = novoUsuario;
	}
	
	public String gerenciar(Usuario usuario) {
		//usuarioAtual = (Usuario) usuarioService.getById(Usuario.class, usuario.getEmail());
		novoUsuario = usuario;
		//usuarioService.closeEntityManager();
		return "gerenciador";
	}

}