package managedbean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import cruzeirao.Gol;
import cruzeirao.Inscricao;
import service.GolService;


@ManagedBean
@SessionScoped

public class GolMB {
	
	private Gol gol = new Gol();
	private Gol golAtual;
	private GolService golService = new GolService();
	private ArrayList<Inscricao> times =  new ArrayList<Inscricao>();
	private ArrayList<Boolean> penaltys =  new ArrayList<Boolean>();
	
	public String salvar() {
		golService.salvar(gol);
		gol = new Gol();
		return "goltabela";
	}
	 

	public List<Gol> getGols() {
		return golService.getGols();
	}

	public Gol getgol() {
		return gol;
	}

	public void setgol(Gol gol) {
		this.gol = gol;
	}
	
	public String gerenciar(Gol gol) {
		golAtual = golService.getGolByTempo(gol.getTempo());
		return "gerenciador";
	}

}