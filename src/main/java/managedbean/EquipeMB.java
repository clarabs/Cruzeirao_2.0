package managedbean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.RowEditEvent;

import cruzeirao.Equipe;
import cruzeirao.Usuario;
import service.EquipeService;

@ManagedBean
@SessionScoped

public class EquipeMB {
	
	private Equipe novaEquipe = new Equipe();
	private EquipeService equipeService = new EquipeService();
	private List<Equipe> equipes;
	
	public String salvar() {
		novaEquipe = equipeService.save(novaEquipe);
		
		if(equipes != null) {
			equipes.add(novaEquipe);
		}
		
		novaEquipe = new Equipe();
		return "equipetabela";
	}
	 

	public void update(RowEditEvent event) {
		
		Equipe e = ((Equipe) event.getObject());
		novaEquipe = equipeService.save(e);
	}
	 

	public List<Equipe> getEquipes() {
		return equipeService.getAll(Equipe.class);
	}

	public Equipe getNovaEquipe() {
		return novaEquipe;
	}

	public void setEquipe(Equipe novaEquipe) {
		this.novaEquipe = novaEquipe;
	}
	
	public void gerenciar(Usuario usuario) {
		this.novaEquipe.setUsuario(usuario);
	}

}
