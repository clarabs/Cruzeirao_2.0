package managedbean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import cruzeirao.Rodada;
import service.RodadaService;

@ManagedBean
@SessionScoped
public class RodadaMB {
	private Rodada novaRodada = new Rodada();
	private RodadaService rodadaService = new RodadaService();
	
	public String salvar() {
		rodadaService.salvar(novaRodada);
		novaRodada = new Rodada();
		return "rodadatabela";
	}
	 
	public List<Rodada> getRodadas() {
		return rodadaService.getRodadas();
	}

	public Rodada getNovaRodada() {
		return novaRodada;
	}

	public void setRodada(Rodada novaRodada) {
		this.novaRodada = novaRodada;
	}
}
