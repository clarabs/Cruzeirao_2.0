package managedbean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import cruzeirao.Inscricao;
import service.InscricaoService;

@ManagedBean
@SessionScoped
public class InscricaoMB {
	private Inscricao novaInscricao = new Inscricao();
	private InscricaoService inscricaoService = new InscricaoService();
	
	public String salvar() {
		inscricaoService.salvar(novaInscricao);
		novaInscricao = new Inscricao();
		return "inscricaotabela";
	}
	 
	public List<Inscricao> getInscricoes() {
		return inscricaoService.getInscricoes();
	}

	public Inscricao getNovaInscricao() {
		return novaInscricao;
	}

	public void setInscricao(Inscricao novaInscricao) {
		this.novaInscricao = novaInscricao;
	}
}
