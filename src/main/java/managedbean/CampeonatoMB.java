package managedbean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.RowEditEvent;

import cruzeirao.Campeonato;
import cruzeirao.Usuario;
import service.CampeonatoService;
import service.UsuarioService;

@ManagedBean
@SessionScoped

public class CampeonatoMB {
	
	private List<Campeonato> campeonatos;
	private Campeonato campeonatoAtual;
	private Campeonato novoCampeonato = new Campeonato();
	private CampeonatoService campeonatoService = new CampeonatoService();
	private Usuario usuarioAtual;
	private UsuarioService usuarioService = new UsuarioService();
	
	
	public String salvar() {
		novoCampeonato = campeonatoService.save(novoCampeonato);
		if(campeonatos != null) {
			campeonatos.add(novoCampeonato);
		}
		
		novoCampeonato = new Campeonato();
		return "campeonatotabela";
	}	 
	
	public void update(RowEditEvent event) {
		
		Campeonato c = ((Campeonato) event.getObject());
		novoCampeonato = campeonatoService.save(c);
	}
	
	public List<Campeonato> getCampeonatos() {
		return campeonatoService.getAll(Campeonato.class);
	}	
	
	public Campeonato getNovoCampeonato() {
		return novoCampeonato;
	}

	public void setNovoCampeonato(Campeonato novoCampeonato) {
		this.novoCampeonato = novoCampeonato;
	}	

	public void gerenciar(Usuario usuario) {
		this.novoCampeonato.setUsuario(usuario);
		//usuarioAtual = (Usuario) usuarioService.getById(Usuario.class, usuario.getEmail());
		//usuarioService.closeEntityManager();
	}
}