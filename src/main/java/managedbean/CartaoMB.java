package managedbean;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import cruzeirao.Cartao;

@ManagedBean
@SessionScoped

public class CartaoMB {
	
	private ArrayList <Cartao> lista = 
			new ArrayList<Cartao>();
	
	private Cartao newcartao = new Cartao();
	private String relatoJuiz;
	
	public String salvar() {
		lista.add(newcartao);
		newcartao = new Cartao();
		return "cartaotabela";
	}
	 

	public ArrayList<Cartao> getLista() {
		return lista;
	}

	public void setLista(ArrayList<Cartao> lista) {
		this.lista = lista;
	}


	public Cartao getCartao() {
		return newcartao;
	}


	public void setCartao(Cartao cartao) {
		this.newcartao = cartao;
	}


	public String getRelatoJuiz() {
		return relatoJuiz;
	}


	public void setRelatoJuiz(String relatoJuiz) {
		this.relatoJuiz = relatoJuiz;
	}


	

}
