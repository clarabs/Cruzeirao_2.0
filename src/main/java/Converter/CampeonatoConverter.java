package Converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import cruzeirao.Campeonato;
import service.Dados;

@FacesConverter("converterCampeonato")
public class CampeonatoConverter implements Converter {

	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {

		
		if (value != null && !value.isEmpty()) {
			
			  for(Campeonato c : Dados.CAMPEONATOS)
				 if(c.getNome().equals(value))
				  	return c;
					
		}

		return null;

	}

	public String getAsString(FacesContext fc, UIComponent uic,
			Object campeonato) {
		if (campeonato == null || campeonato.equals("")) {
			return null;
		} else {
			return ((Campeonato) campeonato).getNome();

		}
	}

}