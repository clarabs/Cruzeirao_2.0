package Converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import cruzeirao.Equipe;
import service.Dados;

@FacesConverter("converterEquipe")
public class EquipeConverter implements Converter {

	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {

		
		if (value != null && !value.isEmpty()) {
			
			  for(Equipe e : Dados.EQUIPES)
				 if(e.getNome().equals(value))
				  	return e;
					
		}

		return null;

	}

	public String getAsString(FacesContext fc, UIComponent uic,
			Object equipe) {
		if (equipe == null || equipe.equals("")) {
			return null;
		} else {
			return ((Equipe) equipe).getNome();

		}
	}

}